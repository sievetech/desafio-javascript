# Desafio JavaScript

### Descrição
Imagine que você é um programador freelancer e um cliente lhe oferece o seguinte serviço.
Uma api oferece acesso a dados de uma caixa de e-mails contendo as seguintes informações: nome, assunto e data de envio. na forma de um JSON, por exemplo.
Seu objetivo é pegar esses dados apresentá-los de alguma forma para o cliente, acompanhados, no entanto, de algumas funcionalidades, assim como:

### Formatação:
Se a data for hoje, só exibe a hora no formato hh:mm, senão, exibe data no formato dd/mm/aaaa.

### Paginação:
Exibir um determinado número de dados por página.

### Ordenação:
* Deve começar em ordem crescente na coluna "Quando".
* Deve ser possível reordenar por outros campos.
* Deve exibir algum marcador para a coluna ordenada e o sentido da ordenação.


## IMPORTANTE!
Sua primeira tarefa será estipular um prazo para a conclusão do serviço.
Bônus

### Filtros:
* Deve ser possível acionar uma busca pelo termo nos campos nome e assunto. A busca deve exibir na página quantos itens foram encontrados.
* Deve ser possível descartar a busca.
* Deve ser possível visualizar só o que foi enviado hoje.

### Observações
* Precisa ser feito com python/django e javascript
* Pode usar plugins de terceiros
* Pesquise no Google a vontade
* Para efeitos de paginação, o input pode ser um json com mais de 10 itens.